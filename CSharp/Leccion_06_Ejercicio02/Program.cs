﻿using System;

namespace Leccion_06_Ejercicio02
{
    class MainClass
    {
        /*Se ingresan tres notas de un alumno, si el promedio es mayor o igual 
         * a siete mostrar un mensaje "Promocionado".
         */
        public static void Main(string[] args)
        {
            float nota1, nota2, nota3, promedio;
            string linea;
            Console.WriteLine("Ingrese una nota");
            linea = Console.ReadLine();
            nota1 = float.Parse(linea);
            Console.WriteLine("Ingrese el segundo valor");
            linea = Console.ReadLine();
            nota2 = float.Parse(linea);
            Console.WriteLine("Ingrese la segunda nota");
            linea = Console.ReadLine();
            nota3 = float.Parse(linea);
            if (((nota1 + nota2 + nota3)/3 )>7)
            {
                Console.WriteLine("Promociona");
            }
            else
            {
                Console.WriteLine("No poromociona");
            }


        }
    }
}
