﻿using System;

namespace Leccion_06_Ejercicio_03
{
    class MainClass
    {
        /*Se ingresa por teclado un número positivo de uno o dos dígitos (1..99) 
         * mostrar un mensaje indicando si el número tiene uno o dos dígitos.
(Tener en cuenta que condición debe cumplirse para tener dos dígitos, un número entero)
        */
        public static void Main(string[] args)
        {
            int num;
            string linea;
            Console.WriteLine("ingrse un numero del 1 al 99:");
            linea = Console.ReadLine();
            num = int.Parse(linea);
            if (num <= 9)
            {
                Console.WriteLine("Tiene un digito");

            }
            else
            {
                Console.WriteLine("Tiene dos digitos");
            }
        }
    }
}
