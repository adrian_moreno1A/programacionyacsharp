﻿using System;

namespace Leccion_05
{
    class MainClass
    {
        /*Se debe desarrollar un programa que pida el ingreso del precio 
         * de un artículo y la cantidad que lleva el cliente. Mostrar lo que debe abonar el comprador.
         */
        public static void Main(string[] args)
        {
            int cantidad;
            float precio, pago;
            string linea;
            Console.WriteLine("Precio:");
            linea = Console.ReadLine();
            precio = float.Parse(linea);
            Console.WriteLine("Cantidad:");
            linea = Console.ReadLine();
            cantidad = int.Parse(linea);
            pago = cantidad * precio;
            Console.WriteLine("Pagas:"+pago);

        }
    }
}
