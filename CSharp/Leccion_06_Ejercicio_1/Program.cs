﻿using System;

namespace Leccion_06_Ejercicio_1
{
    /*Realizar un programa que lea por teclado dos números, 
     * si el primero es mayor al segundo informar su suma y diferencia, 
     * en caso contrario informar el producto y la división del primero respecto al segundo.
     */
    class MainClass
    {
        public static void Main(string[] args)
        {
            int num1, num2, suma, diferencia, producto, division;
            string linea;
            Console.WriteLine("Ingrese un numero:");
            linea = Console.ReadLine();
            num1 = int.Parse(linea);
            Console.WriteLine("Ingrese un segundo numero:");
            linea = Console.ReadLine();
            num2 = int.Parse(linea);
            if (num1 > num2) {

                suma = num1 + num2;
                Console.WriteLine(suma);
                diferencia = num1 - num2;
                Console.WriteLine(diferencia);

            }
            else
            {
                producto = num1 * num2;
                division = num1 / num2;
                Console.WriteLine(producto);
                Console.WriteLine(division);
            }
        }
    }
}
