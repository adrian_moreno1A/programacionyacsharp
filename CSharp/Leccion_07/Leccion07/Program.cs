﻿using System;

namespace Leccion07
{
    /*Se cargan por teclado tres números distintos. Mostrar por pantalla el mayor de ellos.
     */
    class MainClass
    {
        public static void Main(string[] args)
        {
            int num1, num2, num3;
            string linea;

            Console.WriteLine("Numero1:");
            linea = Console.ReadLine();
            num1 = int.Parse(linea);
            Console.WriteLine("Ingrese numero 2");
            linea = Console.ReadLine();
            num2 = int.Parse(linea);
            Console.WriteLine("ingrese el tercer numero");
            linea = Console.ReadLine();
            num3 = int.Parse(linea);
            if (num1>=num2 && num1 >= num3)
            {
                Console.WriteLine(num1);

            }
            else if (num2>=num3 && num2 >= num1)
            {
                Console.WriteLine(num2);
            }
            else
            {
                Console.WriteLine(num3);
            }
        }
    }
}
