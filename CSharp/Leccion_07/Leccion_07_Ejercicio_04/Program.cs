﻿using System;

namespace Leccion_07_Ejercicio_04
{
    class MainClass
    {
        /*Un postulante a un empleo, realiza un test de capacitación, se obtuvo 
         * la siguiente información: cantidad total de preguntas que se le realizaron 
         * y la cantidad de preguntas que contestó correctamente. Se pide confeccionar 
         * un programa que ingrese los dos datos por teclado e informe el nivel del 
         * mismo según el porcentaje de respuestas correctas que ha obtenido, y sabiendo que:
	Nivel máximo:	Porcentaje>=90%.
	Nivel medio:	Porcentaje>=75% y <90%.
	Nivel regular:	Porcentaje>=50% y <75%.
	Fuera de nivel:	Porcentaje<50%.
        */
        public static void Main(string[] args)
        {
            int preguntas, preguntasCorrectas;
            string linea;
            Console.WriteLine("Introduzca el numero total de preguntas: ");
            linea = Console.ReadLine();
            preguntas = int.Parse(linea);
            Console.WriteLine("Inserte el numero de preguntas correctas");
            linea = Console.ReadLine();
            preguntasCorrectas = int.Parse(linea);
            int porcentaje = preguntasCorrectas * 100 / preguntas;
            if (porcentaje >= 90)
            {
                Console.WriteLine("Nivel Maximo");

            }
            else if (porcentaje < 90 && porcentaje > 75)
            {
                Console.WriteLine("Nivel medio");
            }
            else if (porcentaje<75 && porcentaje > 50)
            {
                Console.WriteLine("Nivel regular");
            }
            else 
            {
                Console.WriteLine("Fuera de nivel");
            }
        }
    }
}
