﻿using System;

namespace Leccion_07_Ejercicio03
{
    class MainClass
    {
        /*Confeccionar un programa que permita cargar un número entero 
         * positivo de hasta tres cifras y muestre un mensaje 
         * indicando si tiene 1, 2, o 3 cifras. Mostrar un mensaje de error 
         * si el número de cifras es mayor.
         * */
        public static void Main(string[] args)
        {
            int num1;
            string linea;
            Console.WriteLine("Escriba un numero:");
            linea = Console.ReadLine();
            num1 = int.Parse(linea);
            if (num1 >= 100 && num1<=999)
            {
                Console.WriteLine("Tres cifras");
            }else if (num1>=10 && num1<100)
            {
                Console.WriteLine("Dos cifras");

            }
            else if (num1 <= 9)
            {
                Console.WriteLine("Una cifra");
            }
            else
            {
                Console.WriteLine("Hay mas de tres cifras");
            }
        }
    }
}
