﻿using System;

namespace Leccion_05
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            /*Escribir un programa en el cual se ingresen cuatro números, 
             * calcular e informar la suma de los dos primeros y el producto del tercero y el cuarto.
             */
            int num1, num2, num3, num4, suma, producto;
            string linea;
            Console.WriteLine("Ingrese el primer valor: ");
            linea = Console.ReadLine();
            num1 = int.Parse(linea);
            Console.WriteLine("Ingrese el segundo valor: ");
            linea = Console.ReadLine();
            num2 = int.Parse(linea);
            Console.WriteLine("Ingrese el tercer numero: ");
            linea = Console.ReadLine();
            num3 = int.Parse(linea);
            Console.WriteLine("Ingrese el cuarto valor:");
            linea = Console.ReadLine();
            num4 = int.Parse(linea);
            suma = num1 + num2;
            producto = num3 * num4;
            Console.WriteLine("Suma:" + suma);
            Console.WriteLine("Producto:" + producto);

        }
    }
}
