﻿using System;

namespace Leccion_05
{
    class MainClass
    {
        /*Realizar un programa que lea cuatro valores numéricos e informar su suma y promedio.
         */
        public static void Main(string[] args)
        {
            int num1, num2, num3, num4, suma, promedio;
            string linea;
            Console.WriteLine("Num1:");
            linea = Console.ReadLine();
            num1 = int.Parse(linea);
            Console.WriteLine("Num2:");
            linea = Console.ReadLine();
            num2 = int.Parse(linea);
            Console.WriteLine("Num3:");
            linea = Console.ReadLine();
            num3 = int.Parse(linea);
            Console.WriteLine("Num4:");
            linea = Console.ReadLine();
            num4 = int.Parse(linea);
            suma = num1 + num2 + num3 + num4;
            promedio = suma / 4;
            Console.WriteLine("Suma:" + suma);
            Console.WriteLine("Promedio:" + promedio);


        }
    }
}
