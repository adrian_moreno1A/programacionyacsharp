﻿using System;

namespace Leccion_05
{
    class MainClass
    {

        /*Realizar la carga del lado de un cuadrado, mostrar por pantalla el perímetro 
         * del mismo (El perímetro de un cuadrado se calcula multiplicando el valor del lado por cuatro)
         * */
        public static void Main(string[] args)
        {
            int lado, perimetro;
            string linea;
            Console.WriteLine("Escriba cuanto mide el lado:");
            linea = Console.ReadLine();
            lado = int.Parse(linea);
            perimetro = lado * 4;
            Console.WriteLine("El perimetro es:" + perimetro);
        }
    }
}
